import { containerLayout } from "./helpers/layout.js";
import { switchTheme } from "./helpers/switchTheme.js";
import { createLoadMore } from "./helpers/loadMore.js";
import { fetchData } from "./helpers/fetchData.js";

const init = async () => {
    const { data, message, success } = await fetchData()
    switchTheme()
    containerLayout(data)
    createLoadMore(data)
}

await init()