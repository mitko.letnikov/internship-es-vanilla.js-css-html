import { createCard } from "./createCard.js"

const loadMore = (e, data, cardContainer) => {
    console.log(data)
    const cardElements = document.querySelectorAll(".card")
    console.log(cardElements.length)

    for (let i = cardElements.length; i < cardElements.length + 4; i++) {
        console.log(i)
        if (data[i])
            createCard(data[i], cardContainer)
    }
    if (cardElements.length + 4 >= data.length) {
        const loadMoreBtn = document.querySelector(".load-more")
        loadMoreBtn.remove()
    }
}

export const createLoadMore = (data) => {

    const cardElements = document.querySelectorAll(".card")

    if (cardElements.length >= 4) {
        const body = document.body
        const loadMoreBtn = document.createElement("div")
        const loadbutton = document.createElement("button")
        loadMoreBtn.classList.add("load-more")
        loadMoreBtn.append(loadbutton)
        body.append(loadMoreBtn)
        loadbutton.innerText = "Load More"
        console.log(cardElements)
        const cardContainer = document.querySelector(".card-container")
        loadbutton.addEventListener("click", (e) => loadMore(e, data, cardContainer))
    }
}