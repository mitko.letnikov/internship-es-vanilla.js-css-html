
export async function fetchData() {
    try {
        const response = await fetch('../data.json');
        const data = await response.json();
        // console.log(data);
        return {
            data,
            success: true,
            message: "Data retrieved successfuly"
        }
    } catch (error) {
        console.error('Something went wrong');
        console.error(error);
        return {
            data: [],
            success: false,
            message: "Data retrieval failed"
        }
    }
}