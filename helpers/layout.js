import { createCard } from "./createCard.js";

export const containerLayout = async (data) => {

    const container = document.querySelector(".container")
    const cardContainer = document.createElement("div")
    cardContainer.classList.add("card-container")
    container.append(cardContainer)
    const datasliced = data.slice(0, 4)
    for (let i = 0; i < datasliced.length; i++)
        createCard(datasliced[i], cardContainer)
}