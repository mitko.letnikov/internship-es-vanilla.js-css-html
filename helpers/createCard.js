
const closeModal = (fullscreenModal) => {
    fullscreenModal.remove()
    const body = document.body
    body.style.overflow = 'auto'
}

const enlargeImage = (image) => {
    const body = document.body
    const fullscreenModal = document.createElement("div")
    fullscreenModal.classList.add("full-screen")
    body.append(fullscreenModal)
    body.style.overflow = 'hidden'
    const headerModal = document.createElement("div")
    headerModal.classList.add("header-modal")
    const bodyModal = document.createElement("div")
    bodyModal.classList.add("body-modal")
    fullscreenModal.append(headerModal, bodyModal)

    const closeBtn = document.createElement("button")
    closeBtn.classList.add("close-btn")
    closeBtn.innerText = "X"
    closeBtn.addEventListener("click", () => closeModal(fullscreenModal))

    headerModal.append(closeBtn)

    const imageHolder = document.createElement("div")
    imageHolder.classList.add("image-modal")

    const cardImage = document.createElement("img")
    cardImage.setAttribute("src", image)

    imageHolder.append(cardImage)
    bodyModal.append(imageHolder)
}

export const createCard = (card, cardContainer) => {
    const { image, caption, source_type, liked, date, likes, name, profile_image } = card
    console.log(card)

    const cardDiv = document.createElement("div")
    cardDiv.classList.add("card")

    const cardImageHolder = document.createElement("div")
    const cardImage = document.createElement("img")
    cardImageHolder.classList.add("card-ImageHolder")
    cardImage.classList.add("card-image")
    cardImageHolder.append(cardImage)

    const cardContent = document.createElement("div")
    cardContent.classList.add("card-content")
    cardDiv.append(cardImageHolder, cardContent)

    const cardHeader = document.createElement("div")
    cardHeader.classList.add("card-header")
    cardContent.append(cardHeader)

    const profileImage = document.createElement("img")
    profileImage.classList.add("profile-image")
    const profileInfo = document.createElement("div")
    profileInfo.classList.add("profile-info")
    cardHeader.append(profileImage, profileInfo)

    const nameTag = document.createElement("h4")
    nameTag.classList.add("name")
    const dateTag = document.createElement("span")
    dateTag.classList.add("date")
    profileInfo.append(nameTag, dateTag)

    const captionTag = document.createElement("p")
    captionTag.classList.add("caption")
    cardContent.append(captionTag)

    const cardFooter = document.createElement("div")
    cardFooter.classList.add("card-footer")
    cardContent.append(cardFooter)

    const likesCount = document.createElement("span")
    likesCount.classList.add("likes-count")
    const likeBtn = document.createElement("button")
    likeBtn.classList.add("like-button")
    cardFooter.append(likesCount, likeBtn)

    cardImage.setAttribute("src", image)
    profileImage.setAttribute("src", profile_image)
    nameTag.innerText = name
    dateTag.innerText = date
    captionTag.innerText = caption
    likesCount.innerText = likes
    likeBtn.innerText = "Like"

    cardImage.addEventListener("click", () => enlargeImage(image))

    cardContainer.append(cardDiv)
}