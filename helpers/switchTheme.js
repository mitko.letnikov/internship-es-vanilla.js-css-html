export const switchTheme = () => {
    const switchBox = document.querySelector(".switch input")
    const body = document.body
    console.log(switchBox)
    console.log(body)

    switchBox.addEventListener('click', (e) => {
        console.log(e.target.checked)
        if (e.target.checked) {
            body.style.background = `rgb(0, 0, 0, 0.97)`
        }
        else {
            body.style.background = `#f9fafd`
        }
    })
}